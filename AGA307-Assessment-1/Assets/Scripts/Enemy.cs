using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//public enum eEnemyType 
//{ 
// OneHand,
// TwoHand,
// Archer,
// North,
// West,
// East,
// //---------
// COUNT
// }
public enum eEnemyType
{
    OneHand,
    TwoHand,
    Archer,
    //North,
    //West,
    //East,
    //---------
    COUNT
}

public class Enemy : MonoBehaviour
{
    public eEnemyType myType;
    public float HealthPoints;
    public float damagePerHit = 1f;
    public float HealthtPoints = 1f;
    public float pointsForHit = 1f ;
    // Start is called before the first frame update
    void Start()
    {

        SetUp();
        //for (int i = 0; i < eEnemyType.COUNT; i++)
        //{

        //}
        for (int i = 0; i < (int)eEnemyType.COUNT; i++)
        {
            //if ((eEnemyType)i)/*== eEnemyType.Archer*/
            //{
                switch ((eEnemyType)i) 
                { 
                 // case eEnemyType.North:
                 // break; 
                 // case eEnemyType.West:
                 // break;
                 //case eEnemyType.East:
                 //break;
                case eEnemyType.OneHand:
                    HealthPoints = 50;
                    break;
                case eEnemyType.TwoHand:
                    HealthPoints = 50;
                    break;
                case eEnemyType.Archer:
                    HealthPoints = 50;
                    break;
                default: 
                 break;
                }
            //}
           // Random.onUnitSphere
        }
        

    }
    //eEnmeyType mytype
    public void SetUp()
    {
        switch (myType)
        {
            case eEnemyType.OneHand:
                HealthPoints = 50;
                break;
            case eEnemyType.TwoHand:
                HealthPoints = 40;
                break;
            case eEnemyType.Archer:
                HealthPoints = 60;
                break;
            default:
                break;


        }
        //if (myType == eEnemyType.OneHand)
        //{
        //    HealthPoints = 50;
        //}
        //if (myType == eEnemyType.Archer)
        //{
        //    HealthPoints = 40;
        //}
        //if (myType == eEnemyType.TwoHand) 
        //{
        //    HealthPoints = 60;
        //}
    }

    public void Hit() 
    {
        GameManager.instance.AddPoints(pointsForHit);
        HealthPoints -= damagePerHit;
        if (HealthPoints == 0)
        {
            Die();
        }
    
    
    }
   
    protected virtual float CalculateArmourPen()
    {

        return 0f;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H)) 
        {
            Hit();   //GameManager.instance.AddPoints(1);
        }
       // EnemyManager.instance.StarBuilding(OnFinishedBuilding);
    }
    public void Die()
    {
        GameManager.instance.AddPoints(2);
        EnemyManager.instance.Remove(gameObject);
        Destroy(gameObject);
    }
}
