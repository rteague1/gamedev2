using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class Projectile : MonoBehaviour
{
    public Rigidbody myRigidBody;
    public Transform myTransform;
    public float force = 5f;
    public float active = 6f;
   Target Target;
    //public GameObject projectile;

    // Start is called before the first frame update
    void Start()
    {
        myRigidBody.AddForce(myTransform.forward * force, ForceMode.Impulse);
        Destroy(gameObject, active);
    }

    // Update is called once per frame
    void Update()
    {
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Target")) 
        { 
         other.gameObject.GetComponent<MeshRenderer>().material.color = Color.blue;
            //Destroy(other.gameObject, 2f);
            //Target.OnHit();
            Destroy(gameObject);
        
        }
    }
}
