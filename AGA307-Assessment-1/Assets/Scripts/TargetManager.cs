using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eTargetDifficultySizes {Small , Medium, Large,COUNTT }
public enum eTargetScale { Small , Medium, Large}
public class TargetManager : Singleton<TargetManager>
{
    //public Diffculty diffculty;
    public Transform[] spawnTargetPoints;
    public GameObject[] targetTypes;
    public List<GameObject> targetlist = new List<GameObject> ();
    //public Diffculty diffculty;
    // Start is called before the first frame update
    void Start()
    {
        //for (int i = 0; i < spawnTargetPoints.Length; i++)
        //{
        //    Debug.Log(i + 1);
        //}
        //for (int i = 0; i < spawnTargetPoints.Length; i++)
        //{
        //    var newSpawn = Instantiate(targetTypes[Random.Range(0, targetTypes.Length)], spawnTargetPoints[i].position, spawnTargetPoints[i].rotation);
        //    //enemies.Add(newSpawn);
        //}
        //Debug.Log(target.Count);
        
    }

    GameObject SpawnTarget(GameObject type, Vector3 position, Quaternion  rotation)
    {
        var newSpawn = Instantiate(type, position, rotation);
        targetlist.Add(newSpawn);
        return newSpawn;

    }
    public void Remove(GameObject target)
    {
        if (target == null)
            return;
        if (targetlist.Contains(target))
            targetlist.Remove(target);
       UIManager.instance.SetTargetsLeft(targetlist.Count);
    }
    public void SetTargetsLeft(int count)
    {
        UIManager.instance.SetTargetsLeft(targetlist.Count);
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.L)) 
        {
            {
                for (int i = 0; i < spawnTargetPoints.Length; i++)
                {
                    Debug.Log(i + 1);
                }
               // for (int i = 0; i < spawnTargetPoints.Length; i++)
                //{
                   // var newSpawn = Instantiate(targetTypes[Random.Range(0, targetTypes.Length)], spawnTargetPoints[i].position, spawnTargetPoints[i].rotation);
                    var newSpawn = Instantiate(targetTypes[Random.Range(0, targetTypes.Length)], spawnTargetPoints[Random.Range(0, spawnTargetPoints.Length)].position, spawnTargetPoints[Random.Range(0, spawnTargetPoints.Length)].rotation);
                   // transform.localPosition = Random.onUnitSphere;
                    
                    targetlist.Add(newSpawn);
                    UIManager.instance.SetTargetsLeft(targetlist.Count);
                //}
                Debug.Log(targetlist.Count);
            }

            
        }
        InvokeRepeating("NewTargerGo", 3f, 3f);

    }
    public void NewTargerGo()
    {
        {
            
            var newSpawn  = Instantiate(targetTypes[Random.Range(0, targetTypes.Length)], spawnTargetPoints[Random.Range(0, spawnTargetPoints.Length)].position, spawnTargetPoints[Random.Range(0, spawnTargetPoints.Length)].rotation);
            // transform.localPosition = Random.onUnitSphere;

            targetlist.Add(newSpawn);
            UIManager.instance.SetTargetsLeft(targetlist.Count);
            //}
            CancelInvoke("NewTargerGo");
            Debug.Log(targetlist.Count);
        }
    }
}
