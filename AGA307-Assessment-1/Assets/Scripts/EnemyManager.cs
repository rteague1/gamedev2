using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : Singleton<EnemyManager>
{
    public Transform[] spawnPoints;
    public GameObject[] enemyTypes;
    public List<GameObject> enemies = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 100; i++)
        {
            Debug.Log(i + 1);
        }
        for (int i = 0;i < spawnPoints.Length; i++) 
        {
          var newSpawn = Instantiate(enemyTypes[Random.Range(0, enemyTypes.Length)], spawnPoints[i].position, spawnPoints[i].rotation);
          enemies.Add(newSpawn);
        }
        Debug.Log(enemies.Count);
    }

    //GameObject SpawnEnemy(GameObject type, Vector3 position , Quaternion rotation)
    //{  
    //    spawnEnemy
    
    //}
    GameObject SpawnEnemy(GameObject type, Vector3 position, Quaternion rotation) 
    {
        var newSpawn = Instantiate(type, position, rotation);
        enemies.Add(newSpawn);
        return newSpawn;

    }
    public void Remove(GameObject enemy) 
    {
        if (enemy == null)
            return;
        if (enemies.Contains(enemy))
            enemies.Remove(enemy);
    
    }
    //public void SatrBuilding(System.Action<float> callbackFuctionWhenFinushed)
   // {
   //     callbackFuctionWhenFinushed.Invoke(1f);
   // }
    // Update is called once per frame
    void Update()
    {
        
    }
}
