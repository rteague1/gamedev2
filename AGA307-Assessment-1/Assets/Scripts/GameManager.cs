using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public enum GameState 
{ 
 Start,
 Playing,
 Paused,
 Title,
 GameOver
}

public enum Difficulty {Easy, Medium, Hard, COUNT }
public class GameManager : Singleton<GameManager>
{
    public float score = 0;
    public GameState gameState;
    public Difficulty difficulty;
    public Difficulty currentDiff;
    public float scoreMultriplier = 1f;

    public float gameTimer;
    public float gameTimemax = 30f;

    public void AddPoints(float points)
    {
        score += points * scoreMultriplier;
        UIManager.instance.SetScore(score);

    }

    // Start is called before the first frame update
    void Start()
    {
        currentDiff = Difficulty.Easy;
        gameState = GameState.Title;
         UIManager.instance.SetDiffculty(currentDiff);
        gameTimer = 30f;
        


        difficulty = Difficulty.Easy;//currentDiff;

        SetUp();

    }
    [ContextMenu("CycleDiff")]
    public void Cyclediff()
    {
        var nextDiff = (((int)currentDiff) + 1) % (int)Difficulty.COUNT;
        currentDiff = (Difficulty)nextDiff;
        UIManager.instance.SetDiffculty(currentDiff);

    }
    public void AddTime(float time)
    {
        gameTimer += time;
    }
    public void ChangerGameDifficulty(Difficulty diff)
    {
        currentDiff = diff;
        UIManager.instance.SetDiffculty(currentDiff);

    }
    //public void ChangeGameState(GameState _gameState)
    //{
    //   // gameState = Start;

    //}
    //public void ChangeDifficultyEasy(int Difficulty)
    //{
    //    currentDiff = (Difficulty)Difficulty ;
    //    difficulty = (Difficulty)Difficulty;
    //    UIManager.instance.SetDiffculty(currentDiff);
    //}
    //public void ChangeDifficultyMedium(int Difficulty)
    //{
    //    currentDiff = (Difficulty)Difficulty;s
    //    difficulty = (Difficulty)Difficulty ;
    //    UIManager.instance.SetDiffculty(currentDiff);
    //}
    //public void ChangeDifficultyHard(int Difficulty)
    //{
    //    currentDiff = (Difficulty)Difficulty;
    //    difficulty = (Difficulty)Difficulty;
    //    UIManager.instance.SetDiffculty(currentDiff);
    //}
    public void ChangeDifficultyEasyB()
    {
        currentDiff = Difficulty.Easy;
        difficulty = Difficulty.Easy;
        UIManager.instance.SetDiffculty(currentDiff);
    }
    public void ChangeDifficultyMediumB()
    {
        currentDiff = Difficulty.Medium;
        difficulty = Difficulty.Medium;
        UIManager.instance.SetDiffculty(currentDiff);
    }
    public void ChangeDifficultyHardB()
    {
        currentDiff = Difficulty.Hard;
        difficulty = Difficulty.Hard;
        UIManager.instance.SetDiffculty(currentDiff);
    }
    //public void ChangeDifficultyMedium(int difficulty)
    //{
    //    currentDiff = difficulty;
    //}
    void SetUp()
    {
        switch (difficulty)
        {
            case Difficulty.Easy:
                scoreMultriplier = 1;
                break;
            case Difficulty.Medium:
                scoreMultriplier = 2;
                break;
            case Difficulty.Hard:
                scoreMultriplier = 3;
                break;
            default:
                scoreMultriplier = 1;
                break;

        }

    }
    // Update is called once per frame
    void Update()
    {
       // if (gameState == GameState.Start)
       // {
            if (gameTimer > 0f)
          {
            gameTimer -= Time.deltaTime;
            UIManager.instance.SetTiemrText(gameTimer);
          }
            if (gameTimer < 0f)
           {
              gameTimer = 0f;
            }
        // }
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            ChangeDifficultyEasyB();
        }
        if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            ChangeDifficultyMediumB();
        }
        if (Input.GetKeyUp(KeyCode.Alpha3))
        {
            ChangeDifficultyHardB();
        }
    }
    //public void Timt()
    //{ Timt();
    //    

    //}
}
