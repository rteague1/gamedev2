using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    public TMPro.TextMeshProUGUI scoureText;
    public TMPro.TextMeshProUGUI TargetliefText;
    public TMPro.TextMeshProUGUI diffculyText;
    public TMPro.TextMeshProUGUI timerText;


    public void SetScore(float score)
    {
        scoureText.text = "Score" + score.ToString();
    }

    public void SetTargetsLeft(int count) 
    { 
    
      TargetliefText.text = count.ToString();
    
    }

    public void SetDiffculty(Difficulty diff) 
    {
        diffculyText.text = diff.ToString();
    
    }

    public void SetTiemrText(float time)
    {
        timerText.text = time.ToString();
        
    }
    //public void ChamgeGameDiffiuiculty(int conter) 
    //{ 
    //  GameManager.E
    
    //}

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
