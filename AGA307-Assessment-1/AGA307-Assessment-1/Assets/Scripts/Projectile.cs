using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Rigidbody myRigidBody;
    public Transform myTransform;
    public float force = 5f;
    //public GameObject projectile;

    // Start is called before the first frame update
    void Start()
    {
        myRigidBody.AddForce(myTransform.forward * force, ForceMode.Impulse);
        Destroy(gameObject, 5f);
    }

    // Update is called once per frame
    void Update()
    {
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Target")) 
        { 
         other.gameObject.GetComponent<MeshRenderer>().material.color = Color.blue;
         Destroy(other.gameObject, 3f);
         Destroy(gameObject);
        
        }
    }
}
