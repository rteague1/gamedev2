using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiringPoint : MonoBehaviour
{
	//public Projector projectorPrefad;
	public Projectile projectilePrefad;
	public Transform SpawnPoint;
	public LayerMask targetLayer;
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetButtonDown("Fire1"))
		{
			var newProjectile = Instantiate(projectilePrefad,SpawnPoint.position, Quaternion.LookRotation(SpawnPoint.forward), null);
			//newProjectile.init()
		}

		RaycastHit hitInfo;
		Debug.DrawRay(SpawnPoint.position, SpawnPoint.forward * 100f,Color.green);
		if (Physics.Raycast(SpawnPoint.position, SpawnPoint.forward, out hitInfo, 1000f, targetLayer)) 
		{ 
		  Debug.Log(hitInfo.collider.gameObject.name);
			var target = hitInfo.collider.gameObject.GetComponent<Target>();
			if (target != null)
			{
				//target.OnHit();
			}
		} 
	}
}
